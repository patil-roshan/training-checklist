import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import {CssBaseline, Stack } from "@mui/material";
import FunctionSetInterval from "./FunctionSetInterval";
import {Link} from "react-router-dom";
import PropTypes from "prop-types";
import React from "react";
import ToggleButton from "./ToggleButton";


export default function Navbar(props) {
    return (
        <Box sx={{flexGrow: 1,}} style={{boxShadow:props.mode === "dark" ?"0 5px rgb(34,45,54)":"0 5px #dad6d6",
        }}>
            <AppBar position="static" color="transparent">
                <CssBaseline/>
                <Toolbar>
                    <Typography variant="h5" component="div" sx={{flexGrow: 1}}>
                        <FunctionSetInterval/>
                    </Typography>
                    <div>
                        <Stack direction="row" spacing={4}>
                            <Typography style={{marginTop: "10px"}}>
                                <Link to={"/"} style={{
                                    background: props.mode === "dark" ? "#101418FF" : "#dad6d6",
                                    color: props.mode === "dark" ? "#dad6d6" : "#101418FF",
                                    textDecoration: "none"
                                }}> {props.tag1}</Link>
                            </Typography>
                            <Typography style={{marginTop: "10px"}}>
                                <Link to={"/about"} style={{
                                    background: props.mode === "dark" ? "#101418FF" : "#FFF",
                                    color: props.mode === "dark" ? "#FFF" : "#101418FF",
                                    textDecoration: "none"
                                }}> {props.tag2}</Link>
                            </Typography>
                            <Typography style={{marginTop: "10px"}}>
                                <Link to={"/contact"} style={{
                                    background: props.mode === "dark" ? "#101418FF" : "#FFF",
                                    color: props.mode === "dark" ? "#FFF" : "#101418FF",
                                    textDecoration: "none"
                                }}> {props.tag3} </Link>
                            </Typography>
                            <Typography style={{marginTop: "10px"}}>
                                <Link to={"/projects"} style={{
                                    background: props.mode === "dark" ? "#101418FF" : "#FFF",
                                    color: props.mode === "dark" ? "#FFF" : "#101418FF",
                                    textDecoration: "none"
                                }}> {props.tag4} </Link>
                            </Typography>
                            <Typography style={{marginTop: "10px"}}>
                                <Link to={"/location"} style={{
                                    background: props.mode === "dark" ? "#101418FF" : "#FFF",
                                    color: props.mode === "dark" ? "#FFF" : "#101418FF",
                                    textDecoration: "none"
                                }}> {props.tag5}</Link>
                            </Typography>

                            <ToggleButton mode={props.mode} toggleMode={props.toggleMode}/>
                        </Stack>
                    </div>
                </Toolbar>
            </AppBar>
        </Box>
    );
}


Navbar.propTypes = {
    tag1: PropTypes.string.isRequired,
    tag2: PropTypes.string.isRequired,
    tag3: PropTypes.string.isRequired,
    tag4: PropTypes.string.isRequired,
    tag5: PropTypes.string.isRequired
};

Navbar.defaultProps = {
    tag1: "Home",
    tag2: "tag1",
    tag3: "tag2",
    tag4: "tag3",
    tag5: "tag4",
}