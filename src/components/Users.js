import axios from "axios";
import React, {useEffect, useState} from "react";
import {Grid, Card, CardContent, Typography, Button} from '@mui/material';
import {Container} from "@mui/material";


const baseURL = "https://jsonplaceholder.typicode.com/users";

export default function Users(props) {
    const [isLoading, setIsLoading] = useState(false)
    const [text, setText] = useState('')
    const [showUser, setShowUser] = useState(false)
    const [toggle, setToggle] = useState("Show")
    const [data, setData] = useState([]);
    const handleClick = (abc) => {

        setTimeout(() => {
            setShowUser(!showUser)
            if (toggle === "Show") {
                setToggle("Hide")
            } else {
                setToggle("Show")

            }
            setIsLoading(false)
        }, 2000)
    }


    useEffect(() => {
        axios.get(baseURL)
            .then((response) => {
                const users = response.data;
                setData(users);
            });
    }, []);


    return (
        <Container style={{marginTop: "100px"}}>
            <Button variant="outlined" onClick={() => {
                setIsLoading(true)
                handleClick()
                toggle!=='Show' ? setText('Hidding...') : setText('Showing...')
            }}
                    style={{marginTop: "20px"}}> {toggle} Users
            </Button>
            {isLoading && <p>{text} </p>}

            {showUser &&
                <Container>

                    <Grid container spacing={5} style={{marginTop: "20px"}}>
                        {data.map((user) => (
                            <Grid item key={user.id} xs={12} sm={6} md={4}>
                                <Card sx={{maxWidth: 345}} style={{
                                    border: props.mode === "dark" ? "1px solid #FFF" : "1px solid #101418FF",
                                    padding: "10px",
                                    marginTop: "30px",
                                    background: props.mode === "dark" ? "#101418FF" : "#FFF",
                                    color: props.mode === "dark" ? "#FFF" : "#101418FF",
                                }}>

                                    <CardContent>
                                        <Typography gutterBottom variant="h5" component="div">
                                            {user.name}
                                        </Typography>

                                        <Typography variant="body2"
                                                    style={{color: props.mode === "dark" ? "#dedbdb" : "#101418FF",}}
                                                    align="left">
                                            user name:&nbsp;{user.username} <br/>
                                            user email:&nbsp;{user.email} <br/>
                                            user phone no:&nbsp;{user.phone} <br/>
                                            website:&nbsp;{user.website} <br/>
                                        </Typography>

                                    </CardContent>

                                </Card>
                            </Grid>
                        ))}
                    </Grid>

                </Container>}

        </Container>
    );
}