import React, { lazy, Suspense } from 'react';
import { Container } from "@mui/material";


const Users = lazy(() => import('./Users'))


export default function Home(props) {
    return (
        <Suspense fallback={<div> Loading.... </div>}>
            <h1>Added analytics</h1>
            <Container>
                <Users
                    mode={props.mode}
                    toggleMode={props.toggleMode}
                />
            </Container>
        </Suspense>
    )
}



