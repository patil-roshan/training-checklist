import './App.css';
import { Routes, Route } from 'react-router-dom'

import React, { lazy, Suspense, useEffect, useState } from "react";
import Navbar from "./components/Navbar";
import { GoogleLogin } from '@react-oauth/google';
import { Container } from "@mui/material";

import { initializeApp } from "firebase/app";
import { getAnalytics, logEvent } from "firebase/analytics";



const firebaseConfig = {
    apiKey: "AIzaSyCeGDcRHEIRLdJ60WV82bgtbhUjwP8uAnA",
    authDomain: "techbeas-34ae9.firebaseapp.com",
    projectId: "techbeas-34ae9",
    storageBucket: "techbeas-34ae9.appspot.com",
    messagingSenderId: "465052742908",
    appId: "1:465052742908:web:77cdfcb360c63e2775d379",
    measurementId: "G-65PMC6CFJP"
};
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);


//
// import Home from "./components/Home";
// import About from "./components/About";
// import Contact from "./components/Contact";
// import Projects from "./components/Projects";
// import Location from "./components/Location";
// import Error404 from "./components/errorPages/Error404";


const Home = lazy(() => import('./components/Home'))
const About = lazy(() => import('./components/About'))
const Contact = lazy(() => import('./components/Contact'))
const Projects = lazy(() => import('./components/Projects'))
const Location = lazy(() => import('./components/Location'))
const Error404 = lazy(() => import('./components/errorPages/Error404'))



function App() {

    const [mode, setMode] = useState("dark");


    useEffect(() => {
        console.log("Ananlytics", analytics);
        logEvent(analytics, "page_view");
    }, [])

    const toggleMode = () => {
        if (mode === "dark") {
            setMode("light")
            document.body.style.color = "#101418FF"
            document.body.style.backgroundColor = "#FFF"
        } else {
            setMode("dark")
            document.body.style.color = "#FFF"
            document.body.style.backgroundColor = "#101418FF"
        }
    }

    const responseMessage = (response) => {
        console.log(response);
    };
    const errorMessage = (error) => {
        console.log(error);
    };

    return (
        <>
            {/*<Container maxWidth={"xs"}>*/}
            {/*    <GoogleLogin onSuccess={responseMessage} onError={errorMessage}/>*/}
            {/*</Container>*/}
            <Navbar tag1={"Home"} tag2={"About"} tag3={"Contact"} tag4={"Projects"} tag5={"Location"} mode={mode}
                toggleMode={toggleMode}
            />
            <div className="App App-headers">
                <Suspense fallback={<div>Loading...</div>}>
                    <Routes>
                        <Route path='/' element={<Home mode={mode} toggleMode={toggleMode} />} />
                        <Route path='/about' element={<About />} />
                        <Route path='/contact' element={<Contact />} />
                        <Route path='/projects' element={<Projects />} />
                        <Route path='/location' element={<Location />} />
                        <Route path='/*' element={<Error404 />} />
                    </Routes>
                </Suspense>
            </div>
        </>
    );
}

export default App;
